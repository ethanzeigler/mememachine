package main

import (
	"strings"

	srv "gitlab.com/ethanzeigler/gmbotserver"
	cfg "gitlab.com/ethanzeigler/mememachine/config"
)

func BuildHelpHook(config cfg.MMConfig) *srv.Hook {
	return &srv.Hook{
		Name: "Help Hook",
		GroupIDs: config.GroupIDs(),
		Listener: func(hook *srv.Hook, callback srv.Callback) {
			if !strings.EqualFold(strings.TrimSpace(callback.Text), "/help") {
				return
			}
			msg := srv.Message{BotID: config.IdMap[callback.GroupID]}
			msg.Text = "MemeMachine Commands (v1.0.1):\n" + 
			"    (<required>, [optional], |or|)\n\n" +
			"    /<name>ism [search <query> | record <message>] - Group member quotes\n" +
			"    /ism people - lists all the quoted people in this chat\n" + 
			"    /help - This message"

			hook.PostMessageAsync(msg, 2)
		},
	}
}