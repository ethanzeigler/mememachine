package config

import (
	"encoding/json"
	"io/ioutil"
)

type MMConfig struct {
	IdMap    map[string]string `json:"id_map"`
	DBConStr string            `json:"db_connection_str"`
	Binding     string         `json:"server_binding"`
	GMToken  string            `json:"groupme_token"`
}

func LoadMMConfigFromFile(filename string) (config MMConfig, err error) {
	fileContents, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	err = json.Unmarshal(fileContents, &config)
	return
}

func (config MMConfig) GroupIDs() []string {
	keys := make([]string, len(config.IdMap))

	i := 0
	for k := range config.IdMap {
		keys[i] = k
		i++
	}
	return keys
}