package main

import (
	"fmt"
	srv "gitlab.com/ethanzeigler/gmbotserver"
	"gitlab.com/ethanzeigler/mememachine/config"
	"gitlab.com/ethanzeigler/mememachine/db"
	"gitlab.com/ethanzeigler/mememachine/quotes"

	"os"
)

func main() {
	memeConfig, err := config.LoadMMConfigFromFile("memeconfig.json")
	if err != nil {
		fmt.Printf("Couldn't load config file. Is it in the working dir?: %v\n", err)
		os.Exit(1)
	}
	memeDb, err := db.NewMemeDB(memeConfig.DBConStr)
	if err != nil {
		fmt.Printf("Couldn't create a MemeDB connector\n")
		os.Exit(2)
	}

	botServer := srv.NewGMServer()
	botServer.Config.Binding = memeConfig.Binding
	botServer.Config.GmToken = memeConfig.GMToken

	botServer.Registerhook(quotes.BuildQuoteHook(memeConfig, *memeDb))
	botServer.Registerhook(quotes.BuildUserQuoteSearchHook(memeConfig, *memeDb))
	botServer.Registerhook(quotes.BuildListPeopleHook(memeConfig, *memeDb))
	botServer.Registerhook(BuildHelpHook(memeConfig))

	if len(os.Args[1:]) > 0 && os.Args[1] == "debug" {
		botServer.StartDebug(os.Stdin)
	} else {
		botServer.Start()
	}
}
