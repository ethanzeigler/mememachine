package db

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/ethanzeigler/gmbotserver"
)

func (q Quote) GetPrettyPrint() string {
	date := q.Date.Format("Mon, Jan 2, '06")
	return fmt.Sprintf("%s%s [%s]: %s", strings.ToUpper(string((*q.Name)[0])), (*q.Name)[1:], date, *q.Quote)
}

// gets a random quote from the given user
func (d *MemeDB) GetUserQuote(name string, callback gmbotserver.Callback) (quoteRow Quote, err error) {
	quotes, err := d.GetQuotes(name, callback, 1, RandomSort, false)

	if err != nil {
		return Quote{}, err
	}
	return quotes[0], err
}

func (d *MemeDB) WriteUserQuote(name string, quote string, callback gmbotserver.Callback) error {
	curr := time.Now()
	groupID, err := strconv.Atoi(callback.GroupID)
	if err != nil {
		return err
	}
	_, err = d.db.Exec("INSERT INTO quotes (name, quote, group_id, date, submit_by) VALUES ($1, $2, $3, to_date($4,'YYYY-MM-DD'), $5)",
		strings.ToLower(name), quote, groupID, fmt.Sprintf("%d-%02d-%02d\n",
			curr.Year(), curr.Month(), curr.Day()), callback.SenderID)
	if err != nil {
		return err
	}
	return nil
}

func (d *MemeDB) GetQuotes(name string, callback gmbotserver.Callback, limit int, sortType SortType, ascending bool) (quotes []Quote, err error) {
	groupID, err := strconv.Atoi(callback.GroupID)
	sortStr := "DESC"
	if ascending {
		sortStr = "ASC"
	}
	if err != nil {
		return make([]Quote, 0, 1), err
	}
	var rows *sql.Rows
	switch sortType {
	case DateSort:
		rows, err = d.db.Query("SELECT id, name, quote, group_id, date, submit_by FROM quotes "+
			"WHERE name LIKE $1 AND group_id=$2 ORDER BY date $3 LIMIT $4", strings.ToLower(name), groupID, sortStr, limit)
	case QuoteIDSort:
		rows, err = d.db.Query("SELECT id, name, quote, group_id, date, submit_by FROM quotes "+
			"WHERE name LIKE $1 AND group_id=$2 ORDER BY id $3 LIMIT $4", strings.ToLower(name), groupID, sortStr, limit)
	case RandomSort:
		rows, err = d.db.Query("SELECT id, name, quote, group_id, date, submit_by FROM quotes "+
			"WHERE name LIKE $1 AND group_id=$2 ORDER BY random() LIMIT $3", strings.ToLower(name), groupID, limit)
	default:
		return make([]Quote, 0, 1), errors.New("illegal SortType")
	}

	if err != nil {
		return make([]Quote, 0, 1), err
	}
	var i = 0
	for ; rows.Next(); i++ {
		var name, quote, submitterID string
		var date time.Time
		var quoteID, groupID uint64

		err := rows.Scan(&quoteID, &name, &quote, &groupID, &date, &submitterID)
		if err != nil {
			return make([]Quote, 0, 1), err
		}
		quotes = append(quotes, Quote{
			Name: &name,
			Quote: &quote,
			Date: &date,
			GroupID: &groupID,
			ID: &quoteID,
			SubmitterID: &submitterID,
		})
	}

	if len(quotes) == 0 {
		return make([]Quote, 0, 1), errors.New("no quotes found")
	}
	err = nil
	return
}

func (d *MemeDB) SearchForUserQuoteByText(name string, callback gmbotserver.Callback, searchText string) (quotes []Quote, err error) {
	query := `
	   SELECT id, name, quote, group_id, date, submit_by
	   FROM quotes
	   WHERE
	      name LIKE $1
		  AND group_id = $2
		  AND quote ILIKE '%' || $3 || '%'
	    ORDER BY date DESC
		LIMIT 10;
	`

	rows, err := d.db.Query(query, name, callback.GroupID, searchText)
	if err != nil {
		return make([]Quote, 0, 1), err
	}
	quotes, err = QueryToQuoteSlice(rows)
	return
}

func (d *MemeDB) SearchForPeopleInGroup(callback gmbotserver.Callback) (people map[string]int, err error) {
	query := `
	   SELECT name, COUNT(id)
	   FROM quotes
	   WHERE group_id = $1
	   GROUP BY name
	   ORDER BY name;
	`

	rows, err := d.db.Query(query, callback.GroupID)
	people = make(map[string]int)
	if err != nil {
		return
	}
	for rows.Next() {
		var name string
		var count int

		err = rows.Scan(&name, &count)
		if err != nil {
			return
		}
		people[name] = count
	}
	return
}

// func (d *MemeDB) GetLastRecordedQuote(name string, callback gmbotserver.Callback, limit int, sortType SortType, ascending bool) (sql.Re)

func (d *MemeDB) DeleteQuote(quote Quote) (sql.Result, error) {
	return d.db.Exec("DELETE FROM quotes WHERE id=$1", quote.ID)
}