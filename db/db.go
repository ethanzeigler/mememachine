package db

import (
	"bytes"
	"database/sql"
	_ "github.com/lib/pq"
	"time"
)

type SortType int

const (
	QuoteIDSort SortType = 0
	DateSort    SortType = 1
	RandomSort  SortType = 2
)

// Represents a Row inside of the quote db table
type Quote struct {
	ID          *uint64
	Name        *string
	Quote       *string
	Date        *time.Time
	GroupID     *uint64
	SubmitterID *string
}

func NewMemeDB(conStr string) (*MemeDB, error) {
	var memeDB MemeDB
	var err error
	// open heroku db connection

	//db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	//connStr := "user=bots dbname=bots port=5437 host=127.0.0.1 connect_timeout=5"
	//connStr := "user=bots dbname=bots-debug port=5437 connect_timeout=5 sslmode=disable"
	//conStr := "user=bots dbname=bots connect_timeout=5"
	memeDB.db, err = sql.Open("postgres", conStr)
	if err != nil {
		return nil, err
	} else {
		return &memeDB, nil
	}
}

type MemeDB struct {
	// the database connection
	db *sql.DB
}

func QueryToQuoteSlice(rows *sql.Rows) (quotes []Quote, err error) {
	var i = 0
	for ; rows.Next(); i++ {
		var name, quote, submitterID string
		var date time.Time
		var quoteID, groupID uint64

		err = rows.Scan(&quoteID, &name, &quote, &groupID, &date, &submitterID)
		if err != nil {
			return make([]Quote, 0, 1), err
		}
		quotes = append(quotes, Quote{
			Name: &name,
			Quote: &quote,
			Date: &date,
			GroupID: &groupID,
			ID: &quoteID,
			SubmitterID: &submitterID,
		})
	}
	return
}

func (d *MemeDB) TestQuery(buffer *bytes.Buffer) error {
	rows, err := d.db.Query("SELECT * FROM quotes")
	if err != nil {
		return err
	}

	for i := 0; rows.Next(); i++ {
		row, err := rows.Columns()
		if err != nil {
			buffer.Write([]byte("Cannot query db: "))
			buffer.Write([]byte(err.Error() + "\n"))
		} else {
			buffer.Write([]byte("Row name: "))
			for _, e := range row {
				buffer.Write([]byte(e + "\t"))
			}
			data, _ := rows.ColumnTypes()
			buffer.Write([]byte("\n"))
			buffer.Write([]byte("Row type: "))
			for _, e := range data {
				buffer.Write([]byte(e.ScanType().Name() + "\t"))
			}
			buffer.Write([]byte("\n"))

			var name, quote string
			var date time.Time

			err = rows.Scan(&name, &quote, &date)
			if err != nil {
				buffer.Write([]byte("Could not load data: " + err.Error()))
				return err
			}
			buffer.Write([]byte("Row Data: " + name + "\t" + quote + "\t" + date.String() + "\n"))
		}
		if i > 10 {
			buffer.Write([]byte("Stopping...\n"))
			break
		}
	}
	return nil
}
