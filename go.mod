module gitlab.com/ethanzeigler/mememachine

go 1.16

require (
	github.com/lib/pq v1.10.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/ethanzeigler/gmbotserver v0.1.2
	golang.org/x/sys v0.0.0-20210326220804-49726bf1d181 // indirect
)
