package quotes

import (
	"fmt"
	"regexp"

	"github.com/sirupsen/logrus"
	srv "gitlab.com/ethanzeigler/gmbotserver"
	mdb "gitlab.com/ethanzeigler/mememachine/db"
	cfg "gitlab.com/ethanzeigler/mememachine/config"
)

var listPeopleRegex *regexp.Regexp

func init() {
	listPeopleRegex = regexp.MustCompile(`^(?i)/ism\s+people\s*$`)
}


func BuildListPeopleHook(config cfg.MMConfig, memeDB mdb.MemeDB) *srv.Hook {
	handler := func(hook *srv.Hook, callback srv.Callback) {
		regexMatches := listPeopleRegex.FindStringSubmatch(callback.Text)
		if regexMatches == nil {
			return // not a quote request
		}

		hook.Logger().WithFields(logrus.Fields{
			"group_id": callback.GroupID,
		}).Info("Searching for people within group")

		people, err := memeDB.SearchForPeopleInGroup(callback)
		msg := srv.Message{BotID: config.IdMap[callback.GroupID]}
		if err != nil {
			hook.Logger().WithFields(logrus.Fields{
				"group_id": callback.GroupID,
				"error": err,
			}).Error("Searching for people within group failed")
			msg.Text = "[Error: Reported to developer]: " + err.Error()
			hook.PostMessageAsync(msg, 1000)
			return
		}
		msg.Text += "Person -- count:\n\n"
		for _, key := range(mapKeysStrInt(people)) {
			msg.Text += fmt.Sprintf("%v: %v\n", key, people[key])
		}
		hook.PostMessageAsync(msg, 1000)
	}
	return &srv.Hook{
		Name: "Quote List People Hook",
		GroupIDs: config.GroupIDs(),
		Listener: handler,
	}
}

func mapKeysStrInt(myMap map[string]int) []string {
	keys := make([]string, len(myMap))

	i := 0
	for k := range myMap {
		keys[i] = k
		i++
	}
	return keys
}