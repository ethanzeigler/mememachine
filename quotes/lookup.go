package quotes

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/sirupsen/logrus"
	srv "gitlab.com/ethanzeigler/gmbotserver"
	mdb "gitlab.com/ethanzeigler/mememachine/db"
	cfg "gitlab.com/ethanzeigler/mememachine/config"
)

var lookupRegex *regexp.Regexp

func init() {
	lookupRegex = regexp.MustCompile(`^(?i)/(?P<Name>.+)ism\s+(?P<Subcommand>search)\s+(?P<Argument>.+)\s*$`)
}


func BuildUserQuoteSearchHook(config cfg.MMConfig, memeDB mdb.MemeDB) *srv.Hook {
	handler := func(hook *srv.Hook, callback srv.Callback) {
		regexMatches := lookupRegex.FindStringSubmatch(callback.Text)
		if regexMatches == nil {
			return // not a quote request
		}
		// lets get our stuff straight
		regexNames := lookupRegex.SubexpNames()
		captureGroups := mapSubexpNames(regexMatches, regexNames)
		msg := srv.Message{BotID: config.IdMap[callback.GroupID]}
		name := captureGroups["Name"]
		// subcommand, real := captureGroups["Subcommand"]
		// if real {
		// 	subcommand = strings.TrimSpace(subcommand)
		// } else {
		// 	subcommand = ""
		// }
		argument, real := captureGroups["Argument"]
		if real {
			argument = strings.TrimSpace(argument)
		} else {
			argument = ""
		}

		quotes, err := memeDB.SearchForUserQuoteByText(name, callback, argument)
		if err != nil {
			hook.Logger().WithFields(logrus.Fields{
				"name": name,
				"group_id": callback.GroupID,
				"query": argument,
				"error": err.Error(),
			}).Error("Couldn't query database")
			msg.Text = "[Error: Reported to developer]: " + err.Error()
			hook.PostMessageAsync(msg, 2)
			return
		}

		if len(quotes) == 0 {
			hook.Logger().WithFields(logrus.Fields{
				"name": name,
				"group_id": callback.GroupID,
				"query": argument,
			}).Info("No search results found")
			msg.Text = "I'm not finding anything like that. This search is simple. The exact text must be somewhere in the quote. Double check your input."
			hook.PostMessageAsync(msg, 2)
			return
		}

		hook.Logger().WithFields(logrus.Fields{
			"name": name,
			"group_id": callback.GroupID,
			"query": argument,
			"quotes": quotes,
		}).Info("Search results found")
		
		if len(quotes) == 1 {
			msg.Text = fmt.Sprintf("I found a matching quote...\n\n%s", quotes[0].GetPrettyPrint())
		} else if len(quotes) <= 3 {
			quoteStrings := []string{}
			for _, quote := range quotes {
				quoteStrings = append(quoteStrings, quote.GetPrettyPrint())
			}
			msg.Text = fmt.Sprintf("I found a few matching quotes...\n\n%s", strings.Join(quoteStrings, "\n"))
		} else if len(quotes) < 10 {
			msg.Text = fmt.Sprintf("I found %v matching quotes, too many to show. Can you be more specific?", len(quotes))
		} else {
			msg.Text = "I found 10+ matching quotes, way too many to print out. Can you be more specific?"
		}
		hook.PostMessageAsync(msg, 2)
	}

	return &srv.Hook{
		Name: "Quote Search System",
		GroupIDs: config.GroupIDs(),
		Listener: handler,
	}
}
