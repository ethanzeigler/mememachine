package quotes

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/sirupsen/logrus"
	srv "gitlab.com/ethanzeigler/gmbotserver"
	mdb "gitlab.com/ethanzeigler/mememachine/db"
	cfg "gitlab.com/ethanzeigler/mememachine/config"
)

var quoteRegex *regexp.Regexp

func init() {
	quoteRegex = regexp.MustCompile(`^(?i)/(?P<Name>.+)ism(?:\s+(?P<Subcommand>record|delete|search)\s*(?P<Argument>.+)?|(?P<ImproperData>.*))?\s*$`)
}

// func isImproperCommand(captureGroups map[string]string) bool {
// 	return hasGroup(captureGroups, "ImproperData") ||
// 			(hasGroup(captureGroups, "Subcommand") &&
// 			!hasGroup(captureGroups, "Argument"))
// }

// func isSubcommandAndArg(captureGroups map[string]string) bool {
// 	return hasGroup(captureGroups, "Subcommand") && hasGroup(captureGroups, "Argument")
// }


// func handleSubcommand(captureGroups map[string]string, hook srv.Hook, callback srv.Callback, memeDB mdb.MemeDB, msg srv.Message) {
// 	subcommand := strings.TrimSpace(captureGroups["Subcommand"])
// 	argument := strings.TrimSpace(captureGroups["Argument"])
// 	selectedName := strings.TrimSpace(captureGroups["Name"])

// 	recordQuote := func() {
// 		callback.Server.Log.Debug("Recording Quote for " + selectedName)

// 		// Write quote to the psql db
// 		err := memeDB.WriteUserQuote(selectedName, argument, callback)
// 		if err == nil {
// 			hook.Logger().Debug("Success!")
// 			msg.Text = "👍"
// 			hook.PostMessageAsync(msg, 2)
// 		} else {
// 			hook.Logger().Error("Couldn't record: " + err.Error())
// 			msg.Text = "[Error: Reported to developer] " + err.Error()
// 			hook.PostMessageAsync(msg, 2)
// 		}
// 	}

// 	// =================================
// 	// We need to check for a sub command
// 	// Wow this code is getting big and messy
// 	// =================================

// 	// record subcommand
// 	if strings.EqualFold(subcommand, "record") {
// 		recordQuote()
// 	}
// }


// func BuildQuoteHook(config main.MMConfig, memeDb mdb.MemeDB) *srv.Hook {
// 	quoteRequest := func (hook srv.Hook, callback srv.Callback) {
// 		// check if responsible
// 		matches := quoteRegex.FindStringSubmatch(callback.Text)

// 		// no match?
// 		if matches == nil {
// 			return
// 		}

// 		names := quoteRegex.SubexpNames()
// 		captureGroups := mapSubexpNames(matches, names)

// 		callback.Server.Log.WithFields(logrus.Fields{
// 			"matches":  matches,
// 			"captures": captureGroups,
// 		}).Debug("Recognized quote request")

// 		msg := srv.Message{BotID: config.IdMap[callback.GroupID]}

// 		// is the command used correctly?
// 		if isImproperCommand(captureGroups) {
// 			msg.Text = "Hmm. I don't understand this extra information. Did you want a subcommand? (/commands)"
// 			hook.PostMessageAsync(msg, 2)
// 			return
// 		}

// 		// Get quotee name
// 		selectedName := strings.TrimSpace(captureGroups["Name"])

// 		// subcommand and argument?
// 		if isSubcommandAndArg(captureGroups) {
// 			handleSubcommand(captureGroups, hook, callback, memeDB, msg)

// 			// delete subcommand
// 		} else if !hasGroup(captureGroups, "Subcommand") {
// 			// there isn't a subcommand. Get a quote from the person

// 			quote, err := memeDB.GetUserQuote(selectedName, callback)
// 			if err != nil {
// 				hook.Logger().WithFields(logrus.Fields{
// 					"err":   err.Error(),
// 					"name":  selectedName,
// 					"group": callback.GroupID,
// 				}).Error("Cannot query database")
// 				msg.Text = fmt.Sprint("Cannot get quote: " + err.Error())
// 				hook.PostMessageAsync(msg, 2)
// 				return
// 			}
// 			// write first letter of name
// 			msg.Text += strings.ToUpper(string(selectedName[0]))
// 			// write rest of name and quote
// 			date := quote.Date.Format("Mon, Jan 2, 1970")
// 			msg.Text += fmt.Sprintf("%s [%s]: %s", selectedName[1:], date, *quote.Quote)
// 			hook.PostMessageAsync(msg, 2)
// 			return
// 		}
// 		subcommand := strings.TrimSpace(captureGroups["Subcommand"])

// 		if strings.EqualFold(subcommand, "delete") {
// 			hook.Logger().Debug("Deleting quote")
// 			quote, err := memeDB.GetQuotes(selectedName, callback, 1, mdb.QuoteIDSort)
// 			if err != nil {
// 				if err.Error() == "no quotes found" {
// 					hook.Logger().Debug("Quote doesn't exist")
// 					msg.Text = "Can't delete quote: " + err.Error()
// 					hook.PostMessageAsync(msg, 2)
// 				} else {
// 					hook.Logger().Debug("Failed to connect to db")
// 					msg.Text = "[Error: Reported to developer] " + err.Error()
// 					hook.PostMessageAsync(msg, 2)
// 				}
// 			} else {
// 				// check that it's sent by the person who originally submitted the quote
// 				if *quote[0].SubmitterID == callback.SenderID {
// 					_, err := memeDB.DeleteQuote(quote[0])
// 					if err != nil {
// 						msg.Text = "Couldn't delete that quote: " + err.Error()
// 						hook.PostMessageAsync(msg, 2)
// 					} else {
// 						msg.Text = fmt.Sprintf("Deleted '%s'", *quote[0].Quote)
// 						hook.PostMessageAsync(msg, 2)
// 					}
// 				} else {
// 					// someone else is trying to delete the quote
// 					msg.Text = "Only the person who wrote the quote can delete it"
// 					hook.PostMessageAsync(msg, 2)
// 				}
// 			}
// 		} else {
// 			hook.Logger().Warning("Bad input interpreted as a subcommand")
// 			msg.Text = "Internal error. Misinterpreted the message."
// 			hook.PostMessageAsync(msg, 2)
// 		}
// 		return
// 	}

// 	handleCallback := func (hook *srv.Hook, callback srv.Callback) {
// 		//quoteRequest()
// 	}

// 	return &srv.Hook{
// 		GroupIDs: mapKeys(config.IdMap),
// 		Name: "Quote Hook",
// 		Listener: handleCallback,
// 	}
// }

func BuildQuoteHook(config cfg.MMConfig, memeDB mdb.MemeDB) *srv.Hook {
	handler := func(hook *srv.Hook, callback srv.Callback) {
		regexMatches := quoteRegex.FindStringSubmatch(callback.Text)
		if regexMatches == nil {
			return // not a quote request
		}
		// lets get our stuff straight
		regexNames := quoteRegex.SubexpNames()
		captureGroups := mapSubexpNames(regexMatches, regexNames)
		msg := srv.Message{BotID: config.IdMap[callback.GroupID]}
		name := captureGroups["Name"]
		subcommand, real := captureGroups["Subcommand"]
		if real {
			subcommand = strings.TrimSpace(subcommand)
		} else {
			subcommand = ""
		}
		argument, real := captureGroups["Argument"]
		if real {
			argument = strings.TrimSpace(argument)
		} else {
			argument = ""
		}

		hook.Logger().WithFields(logrus.Fields{
			"matches":  regexMatches,
			"captures": captureGroups,
		}).Debug("Recognized quote request")

		// check that the command itself isn't wonky
		if hasGroup(captureGroups, "ImproperData") {
			msg.Text = "Hmm. I don't understand this extra information. Did you make a mistake? (/help)"
			hook.PostMessageAsync(msg, 2)
			return
		}

		// recording a new quote?
		if strings.EqualFold(subcommand, "record") {
			if argument == "" {
				msg.Text = "Umm... Hello? I can't record silence. (/help)"
				hook.PostMessageAsync(msg, 2)
				return
			}
			hook.Logger().Debug("Recording Quote for " + name)

			// Write quote to the db
			err := memeDB.WriteUserQuote(name, argument, callback)
			if err == nil {
				hook.Logger().Debug("Success!")
				msg.Text = "👍"
				hook.PostMessageAsync(msg, 2)
			} else {
				hook.Logger().Error("Couldn't record: " + err.Error())
				msg.Text = "[Error: Reported to developer] " + err.Error()
				hook.PostMessageAsync(msg, 2)
			}
		} else if strings.EqualFold(subcommand, "search") {
			return //quick workaround
		} else if strings.EqualFold(subcommand, "delete") {
			// temporary disabled
			msg.Text = "Sorry, this is a work in progress"
			hook.PostMessageAsync(msg, 2)
			return





			// hook.Logger().Debug("Deleting quote")
			// quote, err := memeDB.GetQuotes(name, callback, 1, mdb.QuoteIDSort, false)
			// if err != nil {
			// 	if err.Error() == "no quotes found" {
			// 		hook.Logger().Debug("Quote doesn't exist")
			// 		msg.Text = "Can't delete quote: " + err.Error()
			// 		hook.PostMessageAsync(msg, 2)
			// 	} else {
			// 		hook.Logger().Debug("Failed to connect to db")
			// 		msg.Text = "[Error: Reported to developer] " + err.Error()
			// 		hook.PostMessageAsync(msg, 2)
			// 	}
			// } else {
			// 	// check that it's sent by the person who originally submitted the quote
			// 	if *quote[0].SubmitterID == callback.SenderID {
			// 		_, err := memeDB.DeleteQuote(quote[0])
			// 		if err != nil {
			// 			msg.Text = "Couldn't delete that quote: " + err.Error()
			// 			hook.PostMessageAsync(msg, 2)
			// 		} else {
			// 			msg.Text = fmt.Sprintf("Deleted '%s'", *quote[0].Quote)
			// 			hook.PostMessageAsync(msg, 2)
			// 		}
			// 	} else {
			// 		// someone else is trying to delete the quote
			// 		msg.Text = "Only the person who wrote the quote can delete it"
			// 		hook.PostMessageAsync(msg, 2)
			// 	}
			// }













		} else {
			quote, err := memeDB.GetUserQuote(name, callback)
			if err != nil {
				hook.Logger().WithFields(logrus.Fields{
					"err":   err.Error(),
					"name":  name,
					"group": callback.GroupID,
				}).Error("Cannot query database")
				msg.Text = fmt.Sprint("Cannot get quote: " + err.Error())
				hook.PostMessageAsync(msg, 2)
				return
			}
			msg.Text = quote.GetPrettyPrint()
			hook.PostMessageAsync(msg, 2)
			return
		}
	}

	return &srv.Hook{
		Name: "Quote System",
		GroupIDs: mapKeys(config.IdMap),
		Listener: handler,
	}
}

// StackOverflow @thwd
// Map capture group names to values
// Because this isn't native for some reason...
func mapSubexpNames(m, n []string) map[string]string {
	m, n = m[1:], n[1:]
	r := make(map[string]string, len(m))
	for i := range n {
		r[n[i]] = m[i]
	}
	return r
}

func hasGroups(m map[string]string, keys ...string) bool {
	for _, key := range keys {
		if !hasGroup(m, key) {
			return false
		}
	}
	return true
}

// Returns true if the given map contains the given key
func hasGroup(m map[string]string, key string) bool {
	if val, ok := m[key]; ok {
		return len(strings.TrimSpace(val)) > 0
	}
	return false
}

func mapKeys(myMap map[string]string) []string {
	keys := make([]string, len(myMap))

	i := 0
	for k := range myMap {
		keys[i] = k
		i++
	}
	return keys
}